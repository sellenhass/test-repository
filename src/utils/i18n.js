import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import enConfig from "assets/locales/en.json";
import deConfig from "assets/locales/de.json";
import frConfig from "assets/locales/fr.json";
import esConfig from "assets/locales/es.json";
import uaConfig from "assets/locales/ua.json";

i18n.use(initReactI18next).init({
  lng: "en",
  fallbackLng: "en",
  resources: {
    en: { translation: enConfig },
    de: { translation: deConfig },
    fr: { translation: frConfig },
    es: { translation: esConfig },
    ua: { translation: uaConfig },
  },
});

export default i18n;
