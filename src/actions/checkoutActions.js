import {
  ADD_ITEM,
  CHANGE_ITEM_AMOUNT,
  REMOVE_ITEM,
  REMOVE_ALL_ITEMS,
} from "./actionTypes";

export const addItem = (item) => ({ type: ADD_ITEM, item });

export const changeItemAmount = ({ itemCount, id }) => ({
  type: CHANGE_ITEM_AMOUNT,
  itemCount,
  id,
});

export const removeItem = (id) => ({ type: REMOVE_ITEM, id });

export const removeAllItems = () => ({ type: REMOVE_ALL_ITEMS });
