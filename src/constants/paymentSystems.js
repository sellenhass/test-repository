import visaIcon from "assets/images/visa-mc.svg";
import paypalIcon from "assets/images/PayPal.svg";
import coinPaymentsIcon from "assets/images/CoinPayments.svg";
import skrillIcon from "assets/images/Skrill.svg";
import webmoneyIcon from "assets/images/WebMoney.svg";
import alipayIcon from "assets/images/Alipay.svg";

export const PAYMENT_SYSTEMS = [
  { paymentSystem: "visa", icon: visaIcon },
  { paymentSystem: "paypal", icon: paypalIcon },
  { paymentSystem: "coinPayments", icon: coinPaymentsIcon },
  { paymentSystem: "skrill", icon: skrillIcon },
  { paymentSystem: "webmoney", icon: webmoneyIcon },
  { paymentSystem: "alipay", icon: alipayIcon },
];
