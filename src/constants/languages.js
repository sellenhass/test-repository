export const LANGUAGES = {
  en: "eng",
  de: "deu",
  fr: "fra",
  es: "spa",
  ua: "ukr",
};
