export const OPTIONS_BY_ALGORITHM = [
  "SHA-256",
  "Equihash",
  "Ethash",
  "Scrypt",
  "X11",
  "CryptoNight",
];

export const OPTIONS_BY_COIN = ["BTC", "ETP", "PPC", "BTC", "ETP", "PPC"];

export const OPTION_BY_EQUIPMENT = ["Antminer", "Enclosure"];

export const OPTIONS_BY_MANUFACTURER = [
  "AMD",
  "BoundaryElectric",
  "Pandaminer",
  "Nvidia",
];
