export const CHECKOUT_STAGES = {
  CART: "cart",
  BILLING_DETAILS: "billing_details",
  PAYMENT_METHOD: "payment_method",
  SUCCESSFUL_PURCHASE: "successful_purchase",
};
