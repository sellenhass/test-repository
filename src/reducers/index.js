import { combineReducers } from "redux";
import { reducer as form } from "redux-form";
import checkout from "./checkout";

const rootReducer = combineReducers({ checkout, form });

export default rootReducer;
