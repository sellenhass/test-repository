import { checkoutActionTypes } from "actions";

const checkout = (state = [], action) => {
  switch (action.type) {
    case checkoutActionTypes.ADD_ITEM:
      return state.length &&
        state.some((cartItem) => cartItem.item.id === action.item.id)
        ? state.map((cartItem) =>
            cartItem.item.id === action.item.id
              ? { itemCount: cartItem.itemCount + 1, item: cartItem.item }
              : cartItem
          )
        : [...state, { itemCount: 1, item: action.item }];
    case checkoutActionTypes.CHANGE_ITEM_AMOUNT:
      return state.map((cartItem) =>
        cartItem.item.id === action.id && action.itemCount !== 0
          ? { itemCount: action.itemCount, item: cartItem.item }
          : cartItem
      );
    case checkoutActionTypes.REMOVE_ITEM:
      return state.filter((cartItem) => cartItem.item.id !== action.id);
    case checkoutActionTypes.REMOVE_ALL_ITEMS:
      return [];
    default:
      return state;
  }
};

export default checkout;
