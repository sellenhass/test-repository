import React from "react";
import { useTranslation } from "react-i18next";
import { withRouter } from "react-router-dom";
import crossIcon from "assets/images/cross.svg";
import dots from "assets/images/menuDots.svg";

const Menu = ({ history }) => {
  const [t] = useTranslation();

  return (
    <div className="menu">
      <button className="menu__cross" onClick={() => history.push("/")}>
        <img className="menu__cross-icon" src={crossIcon} />
      </button>
      <div className="menu__wrapper">
        <a className="menu__link" href="/">
          {t("menu.home").toUpperCase()}
        </a>
        <img className="menu__dots" src={dots} />
        <a className="menu__link" href="/">
          {t("menu.sell").toUpperCase()}
        </a>
        <img className="menu__dots" src={dots} />
        <a className="menu__link" href="/">
          {t("menu.host").toUpperCase()}
        </a>
        <img className="menu__dots" src={dots} />
        <a className="menu__link" href="/">
          {t("menu.aboutUs").toUpperCase()}
        </a>
        <img className="menu__dots" src={dots} />
        <a className="menu__link" href="/">
          {t("menu.support").toUpperCase()}
        </a>
      </div>
    </div>
  );
};

export default withRouter(Menu);
