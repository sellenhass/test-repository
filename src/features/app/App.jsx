import React from "react";
import { Route, BrowserRouter, Switch, useLocation } from "react-router-dom";
import { Home, Menu } from "features";

const AppSwitch = () => {
  const location = useLocation();
  const isMenu = location.state && location.state.isMenu;

  return (
    <>
      <Switch>
        <Route exact path="/" component={Home} />
      </Switch>
      {isMenu && <Route exact path="/menu" component={Menu} />}
    </>
  );
};

const App = () => (
  <BrowserRouter>
    <AppSwitch />
  </BrowserRouter>
);

export default App;
