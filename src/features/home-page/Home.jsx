import React from "react";
import {
  Header,
  Filters,
  OnSale,
  NewListings,
  News,
  Footer,
} from "./components";

const Home = () => {
  return (
    <>
      <div className="fixed-container">
        <Header />
        <main className="main">
          <div className="container">
            <Filters />
            <OnSale />
            <NewListings />
            <News />
          </div>
        </main>
        <Footer />
      </div>
    </>
  );
};

export default Home;
