import React from "react";
import { PRODUCTS_LIST } from "constants/productsList";
import { ProductCard } from "./components";

const ProductCardList = () => {
  return (
    <ul className="product-list">
      {PRODUCTS_LIST.map((product) => (
        <ProductCard key={product.id} product={product} />
      ))}
    </ul>
  );
};

export default ProductCardList;
