import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { connect } from "react-redux";
import { checkoutActions } from "actions";

import arrowIcon from "assets/images/arrow-mobile.svg";
import starIcon from "assets/images/star.svg";

const ProductCard = ({ product, addToCart }) => {
  const [hovered, setHovered] = useState(false);
  const [t] = useTranslation();

  return (
    <li
      className="product"
      key={product.id}
      onMouseEnter={() => setHovered(!hovered)}
      onMouseLeave={() => setHovered(!hovered)}
    >
      {!hovered ? (
        <>
          <h3 className="product__title">{product.name}</h3>
          <span className="product__spec">{product.mainSpec}</span>
          <div className="product__img-wrapper">
            <img className="product__img" src={product.img} />
          </div>

          <div className="product__props">
            <div className="product__price-wrapper">
              {product.maxPrice ? (
                <>
                  <span className="product__price">{`${product.minPrice.toFixed(
                    2
                  )}`}</span>
                  <span> - </span>
                  <span className="product__price">
                    {product.maxPrice.toFixed(2)}
                  </span>
                </>
              ) : (
                <span className="product__price">
                  {product.minPrice.toFixed(2)}
                </span>
              )}
              {product.previousPrice ? (
                <span className="product__previous-price">
                  {product.previousPrice.toFixed(2)}
                </span>
              ) : null}
              {product.preOrder ? (
                <span className="product__pre-order">
                  {t("onSale.preOrder")}
                </span>
              ) : null}
            </div>

            {product.psu ? (
              <span className="product__psu">{"psu".toUpperCase()}</span>
            ) : null}
          </div>
          <img className="product__open-arrow" src={arrowIcon} />
          {product.favourite && !hovered ? (
            <img className="product__favorite" src={starIcon} />
          ) : null}
        </>
      ) : (
        <>
          <h3 className="product__title">{product.name}</h3>
          <span className="product__spec">{product.mainSpec}</span>
          <div className="product__img-wrapper icon-hover-mobile">
            <img className="product__img blur" src={product.img} />
          </div>
          <div className="blurred-background blur-mobile" />
          <div className="product__border blur-mobile" />
          <div className="hover-wrapper hover-wrapper-mobile">
            <div className="product__props props-hover">
              <div className="product__price-wrapper price-wrapper-mobile">
                {product.maxPrice ? (
                  <>
                    <span className="product__price">{`${product.minPrice.toFixed(
                      2
                    )}`}</span>
                    <span> - </span>
                    <span className="product__price">
                      {product.maxPrice.toFixed(2)}
                    </span>
                  </>
                ) : (
                  <span className="product__price">
                    {product.minPrice.toFixed(2)}
                  </span>
                )}
                {product.previousPrice ? (
                  <span className="product__previous-price">
                    {product.previousPrice.toFixed(2)}
                  </span>
                ) : null}
              </div>
              {product.psu ? (
                <span className="product__psu psu-mobile">
                  {"psu".toUpperCase()}
                </span>
              ) : null}
            </div>

            <div className="border-dots-input">
              <select className="product__select select-mobile">
                <option className="product__option">{`${t(
                  "onSale.hashRate"
                )}: ${product.hashRate}`}</option>
              </select>
            </div>
            <div className="product-btns-wrapper product-btns-wrapper-mobile">
              <button className="details-btn btn-mobile">
                {t("onSale.detailsBtn")}
              </button>
              <button
                className="cart-add-btn btn-mobile"
                onClick={() => {
                  addToCart(product);
                }}
              >
                <div className="border"></div>
                {t("onSale.addToCartBtn")}
              </button>
            </div>
          </div>
        </>
      )}
    </li>
  );
};

export default connect(null, { addToCart: checkoutActions.addItem })(
  ProductCard
);
