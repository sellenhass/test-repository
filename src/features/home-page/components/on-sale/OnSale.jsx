import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { ProductCardList } from "./components";

import gridIcon from "assets/images/gridIcon.svg";
import columnIcon from "assets/images/column.svg";

const OnSale = () => {
  const [t] = useTranslation();
  const [isGrid, setisGrid] = useState(true);

  return (
    <div className="on-sale">
      <div className="on-sale__menu">
        <span className="on-sale__menu-item">
          {t("menu.home").toUpperCase()}
        </span>
        <span className="on-sale__menu-item inactive">
          {t("menu.sell").toUpperCase()}
        </span>
        <span className="on-sale__menu-item inactive">
          {t("menu.host").toUpperCase()}
        </span>
      </div>
      <div className="on-sale__head">
        <h2 className="on-sale__title">{t("onSale.onSale").toUpperCase()}</h2>
        <div className="on-sale__view">
          {isGrid ? (
            <span className="on-sale__text">{`${t("onSale.view")}: ${t(
              "onSale.grid"
            )}`}</span>
          ) : (
            <span className="on-sale__text">{`${t("onSale.view")}: ${t(
              "onSale.column"
            )}`}</span>
          )}
          <button className="on-sale__btn" onClick={() => setisGrid(true)}>
            <img className="on-sale__grid-img" src={gridIcon} />
          </button>
          <button className="on-sale__btn" onClick={() => setisGrid(false)}>
            <img className="on-sale__column-img" src={columnIcon} />
          </button>
        </div>
      </div>

      <div className="on-sale__scroll-bar">
        <ProductCardList />
      </div>
    </div>
  );
};

export default OnSale;
