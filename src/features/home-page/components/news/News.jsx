import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import newsIcon from "assets/images/HashOne.svg";
import newsImg from "assets/images/news-img.svg";

const News = () => {
  const [t] = useTranslation();
  const [clikedBtn1, setClicked1] = useState(false);
  const [clikedBtn2, setClicked2] = useState(false);
  const [clikedBtn3, setClicked3] = useState(false);

  return (
    <div className="news">
      <h2 className="news__title">{t("news.news").toUpperCase()}</h2>
      <div className="news__column">
        <img src={newsIcon} className="news__icon" />
        <a className="news__main-link" href="/">
          {t("news.mainLink").toUpperCase()}
        </a>
        <div className="links-wrapper">
          <a className="news__link" href="/">
            {t("news.link1")}
          </a>
          <a className="news__link" href="/">
            {t("news.link2")}
          </a>
          <a className="news__link" href="/">
            {t("news.link3")}
          </a>
          <a className="news__link" href="/">
            {t("news.link4")}
          </a>
        </div>
        <img src={newsImg} className="news__img" />
        <div className="checkbox__wrapper-absolute">
          <div className="checkbox__wrapper">
            <div className="checkbox" onClick={() => setClicked1(!clikedBtn1)}>
              {clikedBtn1 && <div className="checkbox__dot"></div>}
            </div>
            <div className="checkbox" onClick={() => setClicked2(!clikedBtn2)}>
              {clikedBtn2 && <div className="checkbox__dot"></div>}
            </div>
            <div className="checkbox" onClick={() => setClicked3(!clikedBtn3)}>
              {clikedBtn3 && <div className="checkbox__dot"></div>}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default News;
