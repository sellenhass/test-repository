import React from "react";
import starsIcon from "assets/images/5stars.svg";
import redditIcon from "assets/images/reddit.svg";
import youtubeIcon from "assets/images/youtube.svg";
import fbIcon from "assets/images/fb.svg";
import twitterIcon from "assets/images/twitter.svg";
import fstplaceIcon from "assets/images/1place.svg";
import sndplaceIcon from "assets/images/2place.svg";
import trdplaceIcon from "assets/images/3place.svg";
import fthplaceIcon from "assets/images/4place.svg";

const Footer = () => {
  return (
    <footer className="footer">
      <div className="footer__rating">
        <div className="footer__rating-value">4,9</div>
        <div className="footer__comment-wrapper">
          <span className="footer__comment">Great service and prices!</span>
          <div className="footer__rating-wrapper">
            <img className="footer__stars" src={starsIcon} />
            <span className="footer__author">David Smith</span>
          </div>
        </div>
      </div>

      <div className="footer__sm-links">
        <a href="#" className="footer__link">
          <img className="footer__link-icon" src={redditIcon} />
        </a>
        <a href="#" className="footer__link">
          <img className="footer__link-icon" src={youtubeIcon} />
        </a>
        <a href="#" className="footer__link">
          <img className="footer__link-icon" src={fbIcon} />
        </a>
        <a href="#" className="footer__link">
          <img className="footer__link-icon" src={twitterIcon} />
        </a>
      </div>

      <div className="footer__awards">
        <img className="footer__awards-img" src={fstplaceIcon} />
        <img className="footer__awards-img" src={sndplaceIcon} />
        <img className="footer__awards-img" src={trdplaceIcon} />
        <img className="footer__awards-img" src={fthplaceIcon} />
      </div>
    </footer>
  );
};

export default Footer;
