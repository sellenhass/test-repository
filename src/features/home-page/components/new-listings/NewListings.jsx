import React from "react";
import { useTranslation } from "react-i18next";
import ProductCard from "features/home-page/components/on-sale/components/product-card-list/components/product-card";
import { PRODUCTS_LIST } from "constants/productsList";

const NewListings = () => {
  const [t] = useTranslation();

  return (
    <div className="new-listings">
      <h2 className="new-listings__title">
        {t("newListings.newListings").toUpperCase()}
      </h2>
      <ul>
        {PRODUCTS_LIST.map((newProduct) =>
          newProduct.isNew ? newProduct : false
        )
          .filter((product) => product)
          .map((product) => (
            <ProductCard key={product.id} product={product} />
          ))}
      </ul>
    </div>
  );
};

export default NewListings;
