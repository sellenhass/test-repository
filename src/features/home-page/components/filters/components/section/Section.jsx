import React, { useState } from "react";
import btcIcon from "assets/images/BTC.svg";
import etpIcon from "assets/images/ETP.svg";
import ppcIcon from "assets/images/PPC.svg";
import arrowIcon from "assets/images/arrow.svg";

const iconSelector = (option) => {
  if (option === "BTC") return btcIcon;
  if (option === "ETP") return etpIcon;
  if (option === "PPC") return ppcIcon;
};

const Section = ({ placeholder, options, hasIcon }) => {
  const [opened, setOpened] = useState(false);

  return (
    <div className="section">
      <div className="section__title" onClick={() => setOpened(!opened)}>
        <div className="border"></div>
        {placeholder}
        <img
          src={arrowIcon}
          className={opened ? "arrowIcon opened" : "arrowIcon"}
        />
      </div>
      {opened && (
        <div className="options-wrapper">
          {options.map((option) =>
            hasIcon ? (
              <div className="filter-img-option">
                <div className="border"></div>
                <img
                  className="filter-img-option__icon"
                  src={iconSelector(option)}
                />
                {option}
              </div>
            ) : (
              <div className="filter-option">
                <div className="border"></div>
                {option}
              </div>
            )
          )}
        </div>
      )}
    </div>
  );
};

export default Section;
