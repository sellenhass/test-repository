import React from "react";
import { useTranslation } from "react-i18next";

const MinPrice = () => {
  const [t] = useTranslation();

  return (
    <div className="input-wrapper">
      <input
        className="input-text"
        type="text"
        placeholder={t("filters.minPrice")}
      />
    </div>
  );
};

export default MinPrice;
