import React from "react";
import { useTranslation } from "react-i18next";

const MaxPrice = () => {
  const [t] = useTranslation();
  return (
    <div className="input-wrapper">
      <input
        className="input-text"
        type="text"
        placeholder={t("filters.maxPrice")}
      />
    </div>
  );
};

export default MaxPrice;
