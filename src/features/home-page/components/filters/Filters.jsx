import React from "react";
import { useTranslation } from "react-i18next";
import {
  OPTIONS_BY_ALGORITHM,
  OPTIONS_BY_COIN,
  OPTION_BY_EQUIPMENT,
  OPTIONS_BY_MANUFACTURER,
} from "constants/filterOptions";
import { Section, Search, MinPrice, MaxPrice } from "./components";

const Filters = () => {
  const [t] = useTranslation();

  return (
    <div className="filters">
      <h2 className="filters__title">{t("filters.filters").toUpperCase()}</h2>
      <div className="filters__scroll-bar">
        <div className="filters__list">
          <Section
            options={OPTIONS_BY_ALGORITHM}
            placeholder={t("filters.byAlgorithm")}
          />
          <Section
            hasIcon
            options={OPTIONS_BY_COIN}
            placeholder={t("filters.byCoin")}
          />
          <Section
            options={OPTION_BY_EQUIPMENT}
            placeholder={t("filters.byEquipment")}
          />
          <Section
            options={OPTIONS_BY_MANUFACTURER}
            placeholder={t("filters.byManufacturer")}
          />
          <MinPrice />
          <MaxPrice />
          <Search />
        </div>
      </div>
    </div>
  );
};

export default Filters;
