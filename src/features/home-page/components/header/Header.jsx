import React from "react";
import logo from "assets/images/logo.svg";
import menuMobile from "assets/images/menu-mobile.svg";
import {
  CartButton,
  LangsSelector,
  MenuButton,
  Notifications,
  Purse,
  UserAccount,
} from "./components";
import { Link, useLocation } from "react-router-dom";

const Header = () => {
  let location = useLocation();
  return (
    <header className="header">
      <div className="header__desktop">
        <div className="header__props-left">
          <img src={logo} className="logo-img" />
          <LangsSelector />
        </div>
        <div className="burger-wrapper">
          <MenuButton />
        </div>
        <div className="header__props">
          <UserAccount />
          <Purse />
          <Notifications />
          <CartButton />
        </div>
      </div>
      <div className="header__mobile">
        <Link
          to={{
            pathname: "/menu",
            state: { isMenu: location },
          }}
        >
          <img src={menuMobile} className="menu-icon" />
        </Link>
        <img src={logo} className="logo-img" />
        <CartButton />
      </div>
    </header>
  );
};

export default Header;
