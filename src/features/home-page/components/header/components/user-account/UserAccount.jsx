import React from "react";
import { useTranslation } from "react-i18next";
import accountIcon from "assets/images/account-icon.svg";
import arrowIcon from "assets/images/arrow.svg";

const UserAccount = () => {
  const [t] = useTranslation();
  return (
    <button className="user-account-btn">
      <img className="user-account-btn__icon" src={accountIcon} />
      {t("header.userAccount").toUpperCase()}
      <img className="user-account-btn__arrow" src={arrowIcon} />
    </button>
  );
};

export default UserAccount;
