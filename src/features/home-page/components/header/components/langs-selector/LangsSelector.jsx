import React from "react";
import { useTranslation } from "react-i18next";
import planetIcon from "assets/images/planet.svg";
import { LANGUAGES } from "constants/languages";

const LangsSelector = () => {
  const [, i18n] = useTranslation();
  const onClickHandler = () => {
    if (i18n.language === "en") {
      return i18n.changeLanguage("de");
    }
    if (i18n.language === "de") {
      return i18n.changeLanguage("fr");
    }
    if (i18n.language === "fr") {
      return i18n.changeLanguage("es");
    }
    if (i18n.language === "es") {
      return i18n.changeLanguage("ua");
    }
    if (i18n.language === "ua") {
      return i18n.changeLanguage("en");
    }
  };

  return (
    <div className="langs-selector">
      <img className="langs-selector__icon" src={planetIcon} />
      <span className="langs-selector__name" onClick={onClickHandler}>
        {LANGUAGES[i18n.language].toUpperCase()}
      </span>
    </div>
  );
};

export default LangsSelector;
