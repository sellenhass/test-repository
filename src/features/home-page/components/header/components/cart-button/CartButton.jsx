import React, { useState, useMemo } from "react";
import { connect } from "react-redux";
import { Cart } from "features";
import cartIcon from "assets/images/cart-icon.svg";

const CartButton = ({ checkoutList }) => {
  const [cartOpened, setCartOpened] = useState(false);

  const itemsInCart = useMemo(
    () => checkoutList.reduce((acc, curr) => acc + curr.itemCount, 0),
    [checkoutList]
  );

  return (
    <>
      <button className="cart" onClick={() => setCartOpened(!cartOpened)}>
        <div className="header-dots">
          <div className="header-dots__center"></div>
        </div>
        <div className="prop__wrapper">
          <div className="prop__counter">{itemsInCart || 0}</div>
          <img className="prop__icon" src={cartIcon} />
        </div>
      </button>
      {cartOpened && <Cart onClose={() => setCartOpened(!cartOpened)} />}
    </>
  );
};

export default connect((state) => ({
  checkoutList: state.checkout,
}))(CartButton);
