import React from "react";
import notification from "assets/images/notification-icon.svg";

const Notifications = () => {
  return (
    <button className="prop">
      <div className="header-dots">
        <div className="header-dots__center"></div>
      </div>
      <img className="prop__icon" src={notification} />
    </button>
  );
};

export default Notifications;
