import React from "react";
import { useTranslation } from "react-i18next";
import { Link, useLocation } from "react-router-dom";

const MenuButton = () => {
  const [t] = useTranslation();
  let location = useLocation();

  return (
    <Link
      to={{
        pathname: "/menu",
        state: { isMenu: location },
      }}
    >
      <div className="burger">
        <span className="burger__text">{t("header.menu")}</span>
      </div>
    </Link>
  );
};

export default MenuButton;
