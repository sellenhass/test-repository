import React from "react";
import purseIcon from "assets/images/purse-icon.svg";

const Purse = () => {
  return (
    <button className="prop">
      <div className="header-dots">
        <div className="header-dots__center"></div>
      </div>
      <img className="prop__icon" src={purseIcon} />
    </button>
  );
};

export default Purse;
