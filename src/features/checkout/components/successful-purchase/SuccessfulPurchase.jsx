import React from "react";
import { useTranslation } from "react-i18next";

const SuccessfulPurchase = () => {
  const [t] = useTranslation();

  return (
    <div className="success">
      <span className="success__message">
        {t("success.thankYou")} <span className="success__order">#23542</span>.{" "}
        {t("success.questions")}{" "}
        <a href="/" className="success__support">
          {t("success.here")}
        </a>{" "}
        {t("success.toAsk")}
      </span>
    </div>
  );
};

export default SuccessfulPurchase;
