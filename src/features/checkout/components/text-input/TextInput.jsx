import React from "react";

const TextInput = ({ placeholder, fullSize, input }) => {
  const onChangeHandler = (value) => input.onChange(value);

  return (
    <div className={`checkout__input-wrapper ${fullSize ? "full" : "medium"}`}>
      <input
        type="text"
        placeholder={placeholder}
        className={`checkout__input ${fullSize ? "input-width" : ""}`}
        onChange={onChangeHandler}
      />
    </div>
  );
};

export default TextInput;
