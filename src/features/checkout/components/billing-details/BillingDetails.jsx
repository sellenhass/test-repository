import React, { useState } from "react";
import { Field, reduxForm } from "redux-form";
import { CHECKOUT_STAGES } from "constants/checkoutStages";
import checkIcon from "assets/images/check.svg";
import { TextInput } from "features/checkout/components";
import { useTranslation } from "react-i18next";

const BillingDetails = ({ checkoutStage }) => {
  const [t] = useTranslation();
  const [checked, setChecked] = useState(true);

  return (
    <div>
      {checkoutStage !== CHECKOUT_STAGES.PAYMENT_METHOD && (
        <div className="checkout__title start">
          <span>{t("billing.details")}</span>
          <div
            onClick={() => setChecked(!checked)}
            role="checkbox"
            className="checkout__checkbox-wrapper"
          >
            {checked && (
              <img alt="check" src={checkIcon} className="checkout__checkbox" />
            )}
          </div>
          <span className="checkout__remember">{t("checkout.remember")}</span>
        </div>
      )}
      <form>
        <div
          className={`checkout__input-form ${
            checkoutStage === CHECKOUT_STAGES.PAYMENT_METHOD
              ? "payment-method-height"
              : ""
          }`}
        >
          <Field
            name="firstName"
            component={TextInput}
            placeholder={t("billing.firstName")}
          />
          <Field
            name="lastName"
            component={TextInput}
            placeholder={t("billing.lastName")}
          />
          <Field
            name="companyName"
            component={TextInput}
            placeholder={t("billing.companyName")}
          />
          <Field
            name="country"
            component={TextInput}
            placeholder={t("billing.country")}
          />
          <Field
            name="houseAddress"
            component={TextInput}
            placeholder={t("billing.houseAddress")}
            fullSize
          />
          <Field
            name="houseType"
            component={TextInput}
            placeholder={t("billing.houseType")}
            fullSize
          />
          <Field
            name="city"
            component={TextInput}
            placeholder={t("billing.city")}
          />
          <Field
            name="state"
            component={TextInput}
            placeholder={t("billing.state")}
          />
          <Field
            name="zipCode"
            component={TextInput}
            placeholder={t("billing.zipCode")}
          />
          <Field
            name="phoneNumber"
            component={TextInput}
            placeholder={t("billing.phoneNumber")}
          />
          <Field
            name="email"
            component={TextInput}
            placeholder={t("billing.email")}
          />
          <Field
            name="password"
            component={TextInput}
            placeholder={t("billing.password")}
          />
        </div>
      </form>
    </div>
  );
};

export default reduxForm({ form: "billingDetails" })(BillingDetails);
