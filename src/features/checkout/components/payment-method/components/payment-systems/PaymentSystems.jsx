import React from "react";
import { useDispatch } from "react-redux";
import { change } from "redux-form";
import { PAYMENT_SYSTEMS } from "constants/paymentSystems";

const PaymentSystems = () => {
  const dispatch = useDispatch();
  const onChangeHandler = (method) => {
    dispatch(change("payment", "paymentMethod", method));
  };

  return (
    <div className="payment">
      {PAYMENT_SYSTEMS.map((system) => (
        <div
          className="payment__method"
          onClick={() => onChangeHandler(system.paymentSystem)}
        >
          <img
            alt="visa"
            src={system.icon}
            className={
              system.paymentSystem !== "visa"
                ? "payment__icon"
                : "payment__visa"
            }
          />
        </div>
      ))}
    </div>
  );
};

export default PaymentSystems;
