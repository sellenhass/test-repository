import React, { useState } from "react";
import checkIcon from "assets/images/check.svg";
import { Field, reduxForm } from "redux-form";
import { useTranslation } from "react-i18next";
import { TextInput } from "features/checkout/components";
import { PaymentSystems } from "./components";

const PaymentMethod = () => {
  const [t] = useTranslation();
  const [checked, setChecked] = useState(true);

  return (
    <div>
      <div className="checkout__title start">
        <span>{t("payment.method")}</span>
        <div
          onClick={() => setChecked(!checked)}
          role="checkbox"
          className="checkout__checkbox-wrapper"
        >
          {checked && (
            <img alt="check" src={checkIcon} className="checkout__checkbox" />
          )}
        </div>
        <span className="checkout__remember">{t("checkout.remember")}</span>
      </div>
      <form>
        <div className="checkout__input-form">
          <PaymentSystems />
          <Field
            name="cardNumber"
            component={TextInput}
            placeholder={`XXXX XXXX XXXX XXXX ${t("payment.cardNumber")}`}
            fullSize
          />
          <Field
            name="cardHolder"
            component={TextInput}
            placeholder={`********* ********** ${t("payment.cardHolder")}`}
            fullSize
          />
          <Field
            name="expireDate"
            component={TextInput}
            placeholder={`XX / XX ${t("payment.expireDate")}`}
          />
          <Field
            name="cvv"
            component={TextInput}
            placeholder={`XXX ${t("payment.cvv")}`}
          />
        </div>
      </form>
    </div>
  );
};

export default reduxForm({ form: "payment" })(PaymentMethod);
