import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import checkIcon from "assets/images/check.svg";

const ShippingDetails = () => {
  const [t] = useTranslation();
  const [checked, setChecked] = useState(true);

  return (
    <div className="checkout__title start border-bottom">
      <span>{t("billing.shippingDetails")}</span>
      <div
        onClick={() => setChecked(!checked)}
        role="checkbox"
        className="checkout__checkbox-wrapper"
      >
        {checked && (
          <img alt="check" src={checkIcon} className="checkout__checkbox" />
        )}
      </div>
      <span className="checkout__remember">{t("billing.sameAsBilling")}</span>
    </div>
  );
};

export default ShippingDetails;
