import React, { useMemo } from "react";
import { connect } from "react-redux";
import { checkoutActions } from "actions";
import { CHECKOUT_STAGES } from "constants/checkoutStages";

import sigma from "assets/images/sigma.svg";
import trashIcon from "assets/images/trash-box.svg";
import { useTranslation } from "react-i18next";

const CheckoutControls = ({
  checkout,
  checkoutStage,
  removeAllItems,
  onContinue,
  onBack,
}) => {
  const [t] = useTranslation();
  const totalSum = useMemo(
    () =>
      checkout.reduce(
        (acc, curr) => acc + curr.item.minPrice * curr.itemCount,
        0
      ),
    [checkout]
  );

  return (
    <div className="controls">
      <div className="controls__buttons-wrapper">
        {checkoutStage === CHECKOUT_STAGES.PAYMENT_METHOD && (
          <>
            <button className="controls__button buy" onClick={onContinue}>
              {t("checkout.buy")}
            </button>
            <button className="controls__button" onClick={onBack}>
              {t("checkout.back")}
            </button>
          </>
        )}
        {(checkoutStage === CHECKOUT_STAGES.CART ||
          checkoutStage === CHECKOUT_STAGES.BILLING_DETAILS) && (
          <>
            <button className="controls__button" onClick={onContinue}>
              {t("checkout.continue")}
            </button>
            <button className="controls__button" onClick={removeAllItems}>
              {t("checkout.removeAll")}
            </button>
          </>
        )}
      </div>
      <div className="controls__sum">
        <img alt="sum" src={sigma} className="controls__sigma" />
        <span className="item__price">{totalSum.toFixed(2)}</span>
      </div>
      <div className="controls__buttons-wrapper-mobile">
        <div className="controls__price-mobile">
          <span className="controls__dollar">{totalSum.toFixed(2)}</span>
        </div>
        <button
          className="controls__delete-button-mobile"
          onClick={removeAllItems}
        >
          <img src={trashIcon} className="controls__trash" />
        </button>
        <button className="controls__button" onClick={onContinue}>
          {t("checkout.continue")}
        </button>
      </div>
    </div>
  );
};

export default connect((state) => ({ checkout: state.checkout }), {
  removeAllItems: checkoutActions.removeAllItems,
})(CheckoutControls);
