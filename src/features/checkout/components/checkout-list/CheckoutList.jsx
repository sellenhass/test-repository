import React from "react";
import { CHECKOUT_STAGES } from "constants/checkoutStages";
import { CheckoutItem } from "./components";

const CheckoutList = ({ checkoutList, checkoutStage }) => (
  <div
    className={`list ${
      checkoutStage === CHECKOUT_STAGES.BILLING_DETAILS
        ? "border-bottom billing-stage-height"
        : ""
    } `}
  >
    {checkoutList.map((checkoutItem, index) => (
      <CheckoutItem
        key={checkoutItem.item.id}
        item={checkoutItem.item}
        itemCount={checkoutItem.itemCount}
        lastItem={index + 1 === checkoutList.length}
      />
    ))}
  </div>
);

export default CheckoutList;
