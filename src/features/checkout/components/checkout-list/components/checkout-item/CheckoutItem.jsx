import React, { useState } from "react";
import { connect } from "react-redux";
import { checkoutActions } from "actions";

import arrowIcon from "assets/images/arrow.svg";
import trashIcon from "assets/images/trash-box.svg";

const CheckoutItem = ({
  item,
  itemCount,
  changeItemAmount,
  removeItem,
  lastItem,
}) => {
  const [hovered, setHovered] = useState(false);

  return (
    <div className="item">
      <div
        className="item__image-wrapper"
        onMouseEnter={() => setHovered(!hovered)}
        onMouseLeave={() => setHovered(!hovered)}
        on
      >
        <img
          alt="item image-mobile"
          src={item.img}
          className="item__image-mobile"
        />
        <button
          className="item__delete-item-mobile"
          onClick={() => removeItem(item.id)}
        >
          <img src={trashIcon} className="item__trash" />
        </button>
        {!hovered ? (
          <img alt="item image" src={item.img} className="item__image" />
        ) : (
          <>
            <img alt="item image" src={item.img} className="item__image blur" />
            <div className="item__blurred-background" />
            <img
              onClick={() => removeItem(item.id)}
              alt="delete item"
              src={trashIcon}
              className="item__trash-icon"
            />
          </>
        )}
      </div>
      <div className={`item__info ${lastItem ? "no-border" : ""}`}>
        <div className="item__description-mobile">
          <span>
            <span>{item.name}</span>{" "}
            <span className="item__main-spec">{item.mainSpec}</span>
          </span>
          <span className="item__main-spec-mobile">{item.mainSpec}</span>
          <span className="item__total-price-mobile">
            {(itemCount * item.minPrice).toFixed(2)}
          </span>
        </div>
        <div className="item__amount-and-price">
          <div className="item__amount-controls">
            <div className="item__arrow-wrapper-mobile">
              <img
                onClick={() =>
                  itemCount !== 1
                    ? changeItemAmount({
                        itemCount: --itemCount,
                        id: item.id,
                      })
                    : {}
                }
                src={arrowIcon}
                className="item__arrow-left"
              />
            </div>
            <span className="item__count">
              {itemCount.toString().padStart(2, "0")}
            </span>
            <div className="item__arrow-wrapper-mobile">
              <img
                onClick={() =>
                  changeItemAmount({ itemCount: ++itemCount, id: item.id })
                }
                src={arrowIcon}
                className="item__arrow-right"
              />
            </div>
          </div>
          <span className="item__price">
            {(itemCount * item.minPrice).toFixed(2)}
          </span>
        </div>
      </div>
    </div>
  );
};

export default connect(null, {
  changeItemAmount: checkoutActions.changeItemAmount,
  removeItem: checkoutActions.removeItem,
})(CheckoutItem);
