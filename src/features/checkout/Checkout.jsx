import React, { useMemo, useState } from "react";
import { connect } from "react-redux";
import { CHECKOUT_STAGES } from "constants/checkoutStages";
import OutsideClickHandler from "react-outside-click-handler";
import { useTranslation } from "react-i18next";
import {
  CheckoutList,
  CheckoutControls,
  BillingDetails,
  ShippingDetails,
  PaymentMethod,
  SuccessfulPurchase,
} from "./components";

import crossIcon from "assets/images/cross.svg";
import checkIcon from "assets/images/check.svg";
import logo from "assets/images/logo.svg";
import { CartButton } from "../home-page/components/header/components";

const Checkout = ({ onClose, checkoutList }) => {
  const [t] = useTranslation();

  const [checkoutStage, setCheckoutStage] = useState(CHECKOUT_STAGES.CART);
  const [checked, setChecked] = useState(true);

  const itemsInCart = useMemo(
    () => checkoutList.reduce((acc, curr) => acc + curr.itemCount, 0),
    [checkoutList]
  );

  const continueHandler = () => {
    if (checkoutStage === CHECKOUT_STAGES.CART)
      setCheckoutStage(CHECKOUT_STAGES.BILLING_DETAILS);
    if (checkoutStage === CHECKOUT_STAGES.BILLING_DETAILS)
      setCheckoutStage(CHECKOUT_STAGES.PAYMENT_METHOD);
    if (checkoutStage === CHECKOUT_STAGES.PAYMENT_METHOD)
      setCheckoutStage(CHECKOUT_STAGES.SUCCESSFUL_PURCHASE);
  };

  const backHandler = () => {
    setCheckoutStage(CHECKOUT_STAGES.BILLING_DETAILS);
  };

  return (
    <div className="checkout">
      <OutsideClickHandler onOutsideClick={onClose}>
        <div className="header__mobile">
          <button className="checkout__cross" onClick={onClose}>
            <img src={crossIcon} className="checkout__cross-icon" />
          </button>
          <img src={logo} className="logo-img" />
          <CartButton />
        </div>
        <div
          className={`checkout__stage-indicator ${
            checkoutStage === CHECKOUT_STAGES.BILLING_DETAILS ? "move-left" : ""
          } ${
            checkoutStage === CHECKOUT_STAGES.PAYMENT_METHOD
              ? " move-left-50"
              : ""
          }`}
        >
          <span
            className={`checkout__stage ${
              checkoutStage === CHECKOUT_STAGES.BILLING_DETAILS
                ? "inactive"
                : ""
            } `}
          >
            {t("checkout.cart").toUpperCase()}
          </span>
          <span
            className={`checkout__stage ${
              checkoutStage === CHECKOUT_STAGES.CART ? "inactive" : ""
            } `}
          >
            {t("checkout.billing").toUpperCase()}
          </span>
          <span
            className={`checkout__stage ${
              checkoutStage === CHECKOUT_STAGES.CART ||
              checkoutStage === CHECKOUT_STAGES.BILLING_DETAILS ||
              checkoutStage === CHECKOUT_STAGES.PAYMENT_METHOD
                ? "inactive"
                : ""
            } `}
          >
            {t("checkout.shipping").toUpperCase()}
          </span>
          <span
            className={`checkout__stage ${
              checkoutStage === CHECKOUT_STAGES.BILLING_DETAILS
                ? "inactive"
                : ""
            } `}
          >
            {t("checkout.payment").toUpperCase()}
          </span>
          <span
            className={`checkout__stage ${
              checkoutStage === CHECKOUT_STAGES.PAYMENT_METHOD ? "inactive" : ""
            } `}
          >
            {t("checkout.paymentSuccess").toUpperCase()}
          </span>
        </div>
        <div className="checkout__title">
          <div className="checkout__dialog" />
          {(checkoutStage === CHECKOUT_STAGES.CART ||
            checkoutStage === CHECKOUT_STAGES.BILLING_DETAILS) && (
            <span>{t("checkout.itemsInCart", { itemsInCart })}</span>
          )}
          {checkoutStage === CHECKOUT_STAGES.PAYMENT_METHOD && (
            <div className="checkout__billing-title">
              <span>Enter your Billing details</span>
              <div
                onClick={() => setChecked(!checked)}
                role="checkbox"
                className="checkout__checkbox-wrapper"
              >
                {checked && (
                  <img
                    alt="check"
                    src={checkIcon}
                    className="checkout__checkbox"
                  />
                )}
              </div>
              <span className="checkout__remember">
                {t("checkout.remember")}
              </span>
            </div>
          )}
          {checkoutStage === CHECKOUT_STAGES.SUCCESSFUL_PURCHASE && (
            <span>{t("checkout.success")}</span>
          )}
          <img
            alt="close checkout"
            src={crossIcon}
            onClick={onClose}
            className="checkout__cross"
          />
        </div>
        {checkoutList.length ? (
          <>
            <div className="checkout__desktop">
              {(checkoutStage === CHECKOUT_STAGES.CART ||
                checkoutStage === CHECKOUT_STAGES.BILLING_DETAILS) && (
                <CheckoutList
                  checkoutList={checkoutList}
                  checkoutStage={checkoutStage}
                />
              )}
              {(checkoutStage === CHECKOUT_STAGES.BILLING_DETAILS ||
                checkoutStage === CHECKOUT_STAGES.PAYMENT_METHOD) && (
                <BillingDetails checkoutStage={checkoutStage} />
              )}
              {checkoutStage === CHECKOUT_STAGES.PAYMENT_METHOD && (
                <>
                  <ShippingDetails />
                  <PaymentMethod />
                </>
              )}
              {checkoutStage === CHECKOUT_STAGES.SUCCESSFUL_PURCHASE ? (
                <SuccessfulPurchase />
              ) : (
                <CheckoutControls
                  checkoutStage={checkoutStage}
                  onContinue={continueHandler}
                  onBack={backHandler}
                />
              )}
            </div>
            <div className="checkout__mobile">
              {checkoutStage === CHECKOUT_STAGES.CART && (
                <CheckoutList
                  checkoutList={checkoutList}
                  checkoutStage={checkoutStage}
                />
              )}
              {checkoutStage === CHECKOUT_STAGES.BILLING_DETAILS && (
                <BillingDetails checkoutStage={checkoutStage} />
              )}
              {checkoutStage === CHECKOUT_STAGES.PAYMENT_METHOD && (
                <PaymentMethod />
              )}
              {checkoutStage === CHECKOUT_STAGES.SUCCESSFUL_PURCHASE ? (
                <SuccessfulPurchase />
              ) : (
                <CheckoutControls
                  checkoutStage={checkoutStage}
                  onContinue={continueHandler}
                  onBack={backHandler}
                />
              )}
            </div>
          </>
        ) : (
          <div className="checkout__no-items">{t("checkout.noItems")}</div>
        )}
      </OutsideClickHandler>
    </div>
  );
};

export default connect((state) => ({ checkoutList: state.checkout }))(Checkout);
